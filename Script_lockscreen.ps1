<!-- Windows -->;
<!-- Clement AUGIER de MOUSSAC -->;
<!-- 02/11/2020  -->;
<!-- Ce script verrouille notre écran après un certain temps (défini selon utilisateur grâce à l'argument lock)  -->;

param([Int32]$lock)
Start-Sleep -s $lock
rundll32.exe user32.dll, LockWorkStation
