<!-- Windows -->;
<!-- Clement AUGIER de MOUSSAC -->;
<!-- 02/11/2020  -->;
<!-- Ce script donne des informations sur notre ordintateur  -->;

"Le nom de le machine:"
$env:COMPUTERNAME
""
""

"L'adresse ip:"
Get-NetIPConfiguration | findstr IPv4Address
""
""

"Pour obtenir l'os:" 
(Get-WmiObject -class Win32_OperatingSystem).Caption
""
""   

"Pour l'os version:"
(Get-CimInstance Win32_OperatingSystem).version
""
""

"Date et heure d'allumage:"
$Booted = (Get-WmiObject Win32_OperatingSystem).LastBootUpTime
[Management.ManagementDateTimeConverter]::ToDateTime($Booted)
""
""

"détermine si l'OS est à jour"
[version]$version = (Get-CimInstance CIM_OperatingSystem).Version
If ($Version -gt [version]"6.1.11") {
  echo true
}
ElseIf ($Version -gt [version]"6.1.10") {
  echo false
}
Else {
  echo false
}
""
""

"Espace RAM utilise / Espace RAM dispo : "
$RamTotale = gwmi Win32_OperatingSystem | % {
  $_.TotalVisibleMemorySize
}

$RamDisponible = gwmi Win32_OperatingSystem | % {
  $_.FreePhysicalMemory
}

$s = $RamDisponible / 1000000
echo "Ram disponible =" $s

$RamUtilise = ($RamTotale - $RamDisponible) / 1000000
echo "Ram utilise =" $RamUtilise
""
""

"Espace disque utilise / Espace disque dispo"
"    Pour le disk C"
echo "used (disk C):"
$disk = get-psdrive C
$used_size = $disk.used
write-host ($used_size / 1GB)

echo "available (disk C):"
$disk = get-psdrive C
$used_size = $disk.Free
write-host ($used_size / 1GB)

"    Pour le disk D"
echo "used (disk D):"
$disk = get-psdrive D
$used_size = $disk.used
write-host ($used_size / 1GB)

echo "available (disk D):"
$disk = get-psdrive D
$used_size = $disk.Free
write-host ($used_size / 1GB)
""
""

"Liste des utilisateurs de la machine"
$user = Get-LocalUser
$name = $user.Name
write-host($name)
""
""

"calcule et affiche le temps de reponse moyen vers 8.8.8.8"
$CompName = "8.8.8.8"
foreach ($comp in $CompName) {
  $test = (Test-Connection -ComputerName $comp -Count 1  | measure-Object -Property ResponseTime -Average).average
  $response = ($test -as [int] )
  write-Host "10.33.3.253 average ping time" -NoNewline; write-Host " `"$comp`" is " -NoNewline; ; Write-Host "$response ms" 
}
""
""

