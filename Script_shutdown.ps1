<!-- Windows -->;
<!-- Clement AUGIER de MOUSSAC -->;
<!-- 02/11/2020  -->;
<!-- Ce script éteind notre poste après un certain temps (défini selon utilisateur grâce à l'argument stop)  -->;

param([Int32]$stop)
Start-Sleep -s $stop
Stop-Computer