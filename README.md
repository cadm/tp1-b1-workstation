# Tp1 B1 Workstation

### Host OS

```
PS C:\Users\AUGIER de MOUSSAC> systeminfo

    Nom de l’hôte:                              LAPTOP-UAMOT1MJ
    Nom du système d’exploitation:              Microsoft Windows 10 Famille
    Version du système:                         10.0.19041 N/A version 19041
    [...]
    Type du système:                            x64-based PC
    Processeur(s):                              1 processeur(s) installé(s).
                                                [01] : Intel64 Family 6 Model 142 Stepping 11 GenuineIntel ~1792 MHz
    [...]
    Mémoire physique totale:                    8 042 Mo
    [...]
```


```
C:\Users\AUGIER de MOUSSAC>wmic memorychip get devicelocator, manufacturer
DeviceLocator   Manufacturer

    ChannelA-DIMM0  04CB
    ChannelB-DIMM0  04CB
```



### Devices

```
PS C:\Users\AUGIER de MOUSSAC> Get-WmiObject win32_processor


    Caption           : Intel64 Family 6 Model 142 Stepping 11
    [...]
    Manufacturer      : GenuineIntel
    [...]
    Name              : Intel(R) Core(TM) i7-8565U CPU @ 1.80GHz
    [...]
```

**Explication du nom du processeur :**
Marque → Intel(R) Core(TM) 
Brand Modifier → i7
Generation Indicator → 85
SKU Numeric Digits → 65
Product Line Suffix → U


```
PS C:\Users\AUGIER de MOUSSAC> systeminfo

    [...]
    Processeur(s):                              1 processeur(s) installé(s).
    [...]
```


```
PS C:\Users\AUGIER de MOUSSAC> wmic cpu get NumberOfCores

    NumberOfCores
    4
```


```
PS C:\Users\AUGIER de MOUSSAC> wmic path win32_VideoController get name

    Name
    Intel(R) UHD Graphics 620
    Duet Display
    NVIDIA GeForce MX150
```


```
PS C:\Users\AUGIER de MOUSSAC> wmic diskdrive get model,index,firmwareRevision,status,interfaceType,totalHeads,totalTracks,totalCylinders,totalSectors,partitions

    FirmwareRevision  Index  InterfaceType  Model                      Partitions  Status  TotalCylinders  TotalHeads  TotalSectors  TotalTracks
    M0MA020           1      SCSI           Micron_1100_MTFDDAV256TBN  3           OK      31130           255         500103450     7938150
    02.01A02          0      SCSI           WDC WD10SPZX-21Z10T0       1           OK      121601          255         1953520065    31008255'''
```


```
PS C:\Users\AUGIER de MOUSSAC> diskpart

DISKPART> list disk

      N° disque  Statut         Taille   Libre    Dyn  GPT
      ---------  -------------  -------  -------  ---  ---
      Disque 0    En ligne        931 G octets      0 octets        *
      Disque 1    En ligne        238 G octets    14 M octets        *

DISKPART> select disk 0

    Le disque 0 est maintenant le disque sélectionné.

DISKPART> list partition

      N° partition   Type              Taille   Décalage
      -------------  ----------------  -------  --------
      Partition 1    Principale         931 G   1024 K

DISKPART> select disk 1

    Le disque 1 est maintenant le disque sélectionné.

DISKPART> list partition

      N° partition   Type              Taille   Décalage
      -------------  ----------------  -------  --------
      Partition 1    Système            100 M   1024 K
      Partition 2    Réservé             16 M    101 M
      Partition 3    Principale         237 G    117 M
      Partition 4    Récupération      1024 M    237 G
```


*Pour le disk 0:*
- La Partition 1 est dédiée à stocker nos donées

*Pour le disk 1:*
- La Partition 1 (système) est dédiée uniquement à Windows
- La Partition 2 (réservé) sert à stocker les fichiers de démarrage et se trouvent en début de disque
- La Partition 3 (principale) est dédiée à stocker toutes nos donées
- La Partition 4 (récupération) est dédié à la restauration de windows et résoudre les problèmes système 


### Users

```
PS C:\Users\AUGIER de MOUSSAC> WMIC USERACCOUNT LIST BRIEF

    AccountType  Caption                             Domain           FullName  Name                SID
    512          LAPTOP-UAMOT1MJ\Administrateur      LAPTOP-UAMOT1MJ            Administrateur      S-1-5-21-1285388681-1497559501-3876286197-500
    512          LAPTOP-UAMOT1MJ\AUGIER de MOUSSAC   LAPTOP-UAMOT1MJ            AUGIER de MOUSSAC   S-1-5-21-1285388681-1497559501-3876286197-1001
    512          LAPTOP-UAMOT1MJ\cleme               LAPTOP-UAMOT1MJ            cleme               S-1-5-21-1285388681-1497559501-3876286197-1002
    512          LAPTOP-UAMOT1MJ\cleme_ulo0gy6       LAPTOP-UAMOT1MJ            cleme_ulo0gy6       S-1-5-21-1285388681-1497559501-3876286197-1003
    512          LAPTOP-UAMOT1MJ\DefaultAccount      LAPTOP-UAMOT1MJ            DefaultAccount      S-1-5-21-1285388681-1497559501-3876286197-503
    512          LAPTOP-UAMOT1MJ\Invité              LAPTOP-UAMOT1MJ            Invité              S-1-5-21-1285388681-1497559501-3876286197-501
    512          LAPTOP-UAMOT1MJ\WDAGUtilityAccount  LAPTOP-UAMOT1MJ            WDAGUtilityAccount  S-1-5-21-1285388681-1497559501-3876286197-504
```

L'utilisateur full admin est NT-AUTHORITY\SYSTEM



### Processus

```
PS C:\Users\AUGIER de MOUSSAC> tasklist

    Nom de l’image                 PID Nom de la sessio Numéro de s Utilisation
    ========================= ======== ================ =========== ============
    System Idle Process              0 Services                   0         8 Ko
    System                           4 Services                   0       100 Ko
    Secure System                   72 Services                   0    28 836 Ko
    Registry                       132 Services                   0    42 380 Ko
    smss.exe                       644 Services                   0     1 020 Ko
    csrss.exe                      740 Services                   0     4 056 Ko
    wininit.exe                    840 Services                   0     4 560 Ko
    csrss.exe                      848                            1     3 808 Ko
    services.exe                   912 Services                   0     7 880 Ko
    LsaIso.exe                     932 Services                   0     2 684 Ko
    lsass.exe                      940 Services                   0    18 148 Ko
    svchost.exe                    416 Services                   0     2 800 Ko
    svchost.exe                    852 Services                   0    27 432 Ko
    fontdrvhost.exe                704 Services                   0     2 112 Ko
    svchost.exe                   1108 Services                   0    16 864 Ko
    svchost.exe                   1156 Services                   0     7 848 Ko
    svchost.exe                   1320 Services                   0     6 816 Ko
    svchost.exe                   1388 Services                   0     3 612 Ko
    svchost.exe                   1404 Services                   0     9 028 Ko
    svchost.exe                   1412 Services                   0     9 068 Ko
    svchost.exe                   1420 Services                   0     6 108 Ko
    svchost.exe                   1512 Services                   0     7 152 Ko
    svchost.exe                   1524 Services                   0     6 296 Ko
    svchost.exe                   1636 Services                   0    10 504 Ko
    svchost.exe                   1740 Services                   0     7 820 Ko
    svchost.exe                   1756 Services                   0     6 196 Ko
    svchost.exe                   1776 Services                   0     4 376 Ko
    svchost.exe                   1920 Services                   0    10 832 Ko
    svchost.exe                   1948 Services                   0     8 484 Ko
    svchost.exe                   1536 Services                   0     6 428 Ko
    svchost.exe                   2128 Services                   0     5 604 Ko
    svchost.exe                   2276 Services                   0     5 376 Ko
    svchost.exe                   2332 Services                   0     4 532 Ko
    svchost.exe                   2396 Services                   0     5 664 Ko
    NVDisplay.Container.exe       2460 Services                   0     8 252 Ko
    svchost.exe                   2476 Services                   0     7 504 Ko
    svchost.exe                   2560 Services                   0    10 536 Ko
    svchost.exe                   2588 Services                   0     4 864 Ko
    svchost.exe                   2644 Services                   0    19 012 Ko
    svchost.exe                   2692 Services                   0     8 216 Ko
    svchost.exe                   2704 Services                   0     5 000 Ko
    svchost.exe                   2712 Services                   0     7 772 Ko
    svchost.exe                   2724 Services                   0     3 932 Ko
    svchost.exe                   2816 Services                   0     5 504 Ko
    svchost.exe                   2844 Services                   0     5 288 Ko
    svchost.exe                   2852 Services                   0     6 924 Ko
    Memory Compression            2868 Services                   0   832 940 Ko
    svchost.exe                   2940 Services                   0     6 016 Ko
    igfxCUIService.exe            3040 Services                   0     5 396 Ko
    dasHost.exe                   2608 Services                   0    12 072 Ko
    svchost.exe                   3112 Services                   0     6 100 Ko
    svchost.exe                   3124 Services                   0     7 268 Ko
    svchost.exe                   3336 Services                   0    11 676 Ko
    svchost.exe                   3368 Services                   0     6 420 Ko
    svchost.exe                   3688 Services                   0     5 944 Ko
    svchost.exe                   3692 Services                   0     7 740 Ko
    svchost.exe                   3704 Services                   0     4 716 Ko
    svchost.exe                   3896 Services                   0     7 196 Ko
    svchost.exe                   3992 Services                   0    13 232 Ko
    svchost.exe                   4088 Services                   0     9 340 Ko
    spoolsv.exe                   3916 Services                   0    10 568 Ko
    wlanext.exe                   4020 Services                   0     3 832 Ko
    conhost.exe                   4100 Services                   0     3 080 Ko
    svchost.exe                   4164 Services                   0     9 720 Ko
    svchost.exe                   4448 Services                   0    12 524 Ko
    ACCSvc.exe                    4456 Services                   0     7 452 Ko
    mDNSResponder.exe             4464 Services                   0     4 352 Ko
    IntelCpHDCPSvc.exe            4480 Services                   0     4 160 Ko
    KiteService.exe               4496 Services                   0    23 352 Ko
    NvTelemetryContainer.exe      4504 Services                   0     4 584 Ko
    RtkAudUService64.exe          4512 Services                   0     6 124 Ko
    RstMwService.exe              4520 Services                   0     4 692 Ko
    PsyFrameGrabberService.ex     4528 Services                   0     5 560 Ko
    svchost.exe                   4540 Services                   0     3 816 Ko
    svchost.exe                   4536 Services                   0    35 888 Ko
    vk_service.exe                4552 Services                   0     5 808 Ko
    svchost.exe                   4560 Services                   0    17 824 Ko
    svchost.exe                   4568 Services                   0     3 880 Ko
    svchost.exe                   4576 Services                   0    22 128 Ko
    svchost.exe                   4584 Services                   0     3 792 Ko
    XperiaCompanionService.ex     4592 Services                   0     3 896 Ko
    svchost.exe                   4600 Services                   0     6 468 Ko
    sqlwriter.exe                 4612 Services                   0     4 536 Ko
    MsMpEng.exe                   4624 Services                   0   373 888 Ko
    svchost.exe                   4636 Services                   0    16 340 Ko
    mysqld.exe                    4672 Services                   0     4 316 Ko
    svchost.exe                   4936 Services                   0     5 968 Ko
    svchost.exe                   4108 Services                   0     5 524 Ko
    IntelCpHeciSvc.exe            4216 Services                   0     4 140 Ko
    svchost.exe                   5272 Services                   0     3 800 Ko
    svchost.exe                   5280 Services                   0     7 720 Ko
    svchost.exe                   5296 Services                   0     7 012 Ko
    jhi_service.exe               5728 Services                   0     3 896 Ko
    mysqld.exe                    5788 Services                   0    16 036 Ko
    conhost.exe                   5804 Services                   0     3 836 Ko
    WmiPrvSE.exe                  6084 Services                   0     7 904 Ko
    NisSrv.exe                    4288 Services                   0     7 852 Ko
    PresentationFontCache.exe     7232 Services                   0     5 316 Ko
    DropboxUpdate.exe             7420 Services                   0     3 476 Ko
    svchost.exe                   7512 Services                   0     4 700 Ko
    svchost.exe                   7524 Services                   0    14 640 Ko
    svchost.exe                   7452 Services                   0    17 172 Ko
    svchost.exe                   8320 Services                   0     5 996 Ko
    svchost.exe                   8788 Services                   0    14 924 Ko
    svchost.exe                   8872 Services                   0    14 508 Ko
    MoUsoCoreWorker.exe           8976 Services                   0    48 296 Ko
    svchost.exe                   8768 Services                   0    21 188 Ko
    SearchIndexer.exe             9504 Services                   0    60 956 Ko
    svchost.exe                   8048 Services                   0    13 472 Ko
    svchost.exe                   8288 Services                   0     6 592 Ko
    svchost.exe                  11684 Services                   0     6 600 Ko
    SecurityHealthService.exe    11972 Services                   0    12 236 Ko
    svchost.exe                  13208 Services                   0     7 920 Ko
    QASvc.exe                     5896 Services                   0     8 364 Ko
    svchost.exe                  13044 Services                   0     7 456 Ko
    svchost.exe                  15340 Services                   0    13 896 Ko
    GoProDeviceDetection.exe     12768 Services                   0    13 576 Ko
    IAStorDataMgrSvc.exe         13940 Services                   0    48 624 Ko
    SgrmBroker.exe               13684 Services                   0     7 688 Ko
    svchost.exe                    168 Services                   0     7 144 Ko
    DbxSvc.exe                   12104 Services                   0     4 012 Ko
    svchost.exe                   7948 Services                   0     5 856 Ko
    AppleMobileDeviceService.    15840 Services                   0     9 636 Ko
    svchost.exe                  13228 Services                   0     5 148 Ko
    csrss.exe                     4336                            3     5 288 Ko
    csrss.exe                     9044 Console                    4     6 916 Ko
    winlogon.exe                  2272 Console                    4    10 088 Ko
    fontdrvhost.exe               4796 Console                    4     8 060 Ko
    dwm.exe                      18836 Console                    4    75 548 Ko
    NVDisplay.Container.exe      14356 Console                    4    27 944 Ko
    sihost.exe                   11008 Console                    4    29 236 Ko
    svchost.exe                  17460 Console                    4    42 480 Ko
    igfxEM.exe                   13224 Console                    4    23 672 Ko
    svchost.exe                  17704 Console                    4    47 716 Ko
    taskhostw.exe                 4912 Console                    4    21 300 Ko
    explorer.exe                 11292 Console                    4   158 868 Ko
    svchost.exe                  14084 Console                    4    23 992 Ko
    StartMenuExperienceHost.e     6256 Console                    4    69 192 Ko
    dllhost.exe                  19312 Console                    4    12 456 Ko
    RuntimeBroker.exe            15280 Console                    4    25 932 Ko
    SearchApp.exe                 4656 Console                    4    99 244 Ko
    RuntimeBroker.exe            15460 Console                    4    45 844 Ko
    YourPhone.exe                17136 Console                    4     2 120 Ko
    SettingSyncHost.exe          11104 Console                    4     6 104 Ko
    ctfmon.exe                    9116 Console                    4    26 088 Ko
    RuntimeBroker.exe            18080 Console                    4    26 116 Ko
    RuntimeBroker.exe            10704 Console                    4    14 024 Ko
    YourPhoneServer.exe          10716 Console                    4    15 584 Ko
    ShellExperienceHost.exe       9140 Console                    4    60 072 Ko
    RuntimeBroker.exe             5948 Console                    4    25 020 Ko
    svchost.exe                   9532 Console                    4    18 092 Ko
    SecurityHealthSystray.exe     1904 Console                    4     8 596 Ko
    Skype.exe                     2756 Console                    4    61 916 Ko
    Skype.exe                    13664 Console                    4    24 700 Ko
    Skype.exe                    20336 Console                    4    53 916 Ko
    Skype.exe                    10020 Console                    4    31 272 Ko
    Skype.exe                     5436 Console                    4    58 416 Ko
    chrome.exe                    1720 Console                    4   240 656 Ko
    chrome.exe                    9624 Console                    4     6 684 Ko
    Cortana.exe                  19572 Console                    4    62 952 Ko
    chrome.exe                    7336 Console                    4    74 824 Ko
    chrome.exe                   19364 Console                    4    50 520 Ko
    TekVisaRM.exe                 8508 Console                    4    13 776 Ko
    chrome.exe                   18428 Console                    4    55 588 Ko
    Dropbox.exe                   9720 Console                    4   129 700 Ko
    Dropbox.exe                  19324 Console                    4     8 332 Ko
    Dropbox.exe                   6504 Console                    4    11 212 Ko
    RuntimeBroker.exe             3460 Console                    4    28 652 Ko
    SpeechRuntime.exe              444 Console                    4    17 360 Ko
    svchost.exe                   2000 Console                    4    19 716 Ko
    chrome.exe                   12440 Console                    4   113 236 Ko
    Discord.exe                  10756 Console                    4    71 216 Ko
    Discord.exe                   9868 Console                    4    93 928 Ko
    Discord.exe                  19376 Console                    4    39 328 Ko
    Discord.exe                  13864 Console                    4    50 272 Ko
    Discord.exe                  11268 Console                    4   177 924 Ko
    RtkAudUService64.exe         13264 Console                    4     2 188 Ko
    QAAgent.exe                   9896 Console                    4     9 228 Ko
    VirusKeeper.exe               8912 Console                    4    68 448 Ko
    QAAdminAgent.exe             11072 Console                    4    13 456 Ko
    igfxext.exe                  12828 Console                    4     8 812 Ko
    unsecapp.exe                  8496 Console                    4     8 020 Ko
    Discord.exe                  17772 Console                    4    55 688 Ko
    QtWebEngineProcess.exe        8136 Console                    4    45 168 Ko
    QALockHandler.exe            10496 Console                    4     8 400 Ko
    unsecapp.exe                 10112 Console                    4     8 016 Ko
    QtWebEngineProcess.exe        5944 Console                    4    32 824 Ko
    SystemSettings.exe            9036 Console                    4     1 872 Ko
    ApplicationFrameHost.exe      7248 Console                    4    37 596 Ko
    Calculator.exe               16752 Console                    4     1 800 Ko
    IAStorIcon.exe                  76 Console                    4    35 580 Ko
    RuntimeBroker.exe            15144 Console                    4    24 012 Ko
    WinStore.App.exe             14744 Console                    4     1 996 Ko
    RuntimeBroker.exe            18224 Console                    4    17 204 Ko
    WWAHost.exe                  15288 Console                    4     2 724 Ko
    dllhost.exe                  18108 Console                    4    13 332 Ko
    vk_oascan.exe                11420 Console                    4    17 636 Ko
    ePowerButton_NB.exe          12052 Console                    4     4 892 Ko
    ACCStd.exe                   14776 Console                    4    11 192 Ko
    vk_watchop.exe               11012 Console                    4     8 400 Ko
    LockApp.exe                  19764 Console                    4    50 012 Ko
    RuntimeBroker.exe            17556 Console                    4    33 384 Ko
    TextInputHost.exe             5128 Console                    4    42 604 Ko
    chrome.exe                   16240 Console                    4    43 748 Ko
    Microsoft.Msn.Weather.exe     4744 Console                    4     1 820 Ko
    RuntimeBroker.exe            20296 Console                    4    17 780 Ko
    UserOOBEBroker.exe           12424 Console                    4     8 500 Ko
    svchost.exe                   2500 Console                    4     8 024 Ko
    SystemSettingsBroker.exe     18268 Console                    4    29 616 Ko
    chrome.exe                   10332 Console                    4    16 868 Ko
    chrome.exe                   17536 Console                    4   102 448 Ko
    svchost.exe                  12084 Services                   0    20 412 Ko
    kited.exe                    17076 Console                    4   125 468 Ko
    taskhostw.exe                17280 Console                    4    16 772 Ko
    Code.exe                      4840 Console                    4    67 044 Ko
    Code.exe                      7876 Console                    4    22 200 Ko
    Code.exe                      5016 Console                    4    94 464 Ko
    Code.exe                     17372 Console                    4    36 944 Ko
    Code.exe                     12240 Console                    4   138 324 Ko
    Code.exe                     16232 Console                    4    90 072 Ko
    WmiPrvSE.exe                 12596 Services                   0    17 224 Ko
    svchost.exe                   7928 Services                   0     8 916 Ko
    Code.exe                      2096 Console                    4    73 728 Ko
    chrome.exe                   12644 Console                    4    53 196 Ko
    chrome.exe                   14056 Console                    4    38 432 Ko
    powershell.exe               18288 Console                    4    80 732 Ko
    conhost.exe                   1432 Console                    4    17 872 Ko
    Microsoft.Photos.exe         19120 Console                    4    48 452 Ko
    svchost.exe                   8120 Services                   0     6 908 Ko
    duet.exe                      6568 Console                    4    42 532 Ko
    chrome.exe                   14064 Console                    4   229 388 Ko
    chrome.exe                   18260 Console                    4    38 460 Ko
    chrome.exe                    2004 Console                    4    75 948 Ko
    audiodg.exe                   7060 Services                   0    16 272 Ko
    vds.exe                      11596 Services                   0    11 240 Ko
    RuntimeBroker.exe            16296 Console                    4    14 024 Ko
    svchost.exe                  14480 Services                   0    15 288 Ko
    svchost.exe                  15152 Services                   0     5 140 Ko
    svchost.exe                  11756 Services                   0     7 460 Ko
    chrome.exe                   16480 Console                    4   342 516 Ko
    commsapps.exe                15400 Console                    4     2 028 Ko
    RuntimeBroker.exe            16056 Console                    4    21 944 Ko
    HxTsr.exe                     3492 Console                    4     7 548 Ko
    chrome.exe                    7432 Console                    4    72 432 Ko
    chrome.exe                   18128 Console                    4    36 164 Ko
    rundll32.exe                  2176 Console                    4     8 628 Ko
    chrome.exe                   10876 Console                    4    83 920 Ko
    chrome.exe                   16912 Console                    4    75 144 Ko
    chrome.exe                   10576 Console                    4    67 032 Ko
    chrome.exe                   19116 Console                    4    58 296 Ko
    chrome.exe                    8112 Console                    4    62 732 Ko
    chrome.exe                   10912 Console                    4    91 516 Ko
    chrome.exe                    6260 Console                    4    76 676 Ko
    chrome.exe                    1828 Console                    4    84 584 Ko
    chrome.exe                    7772 Console                    4    60 736 Ko
    chrome.exe                   15956 Console                    4    97 196 Ko
    chrome.exe                    5460 Console                    4    88 212 Ko
    svchost.exe                  13628 Services                   0    12 256 Ko
    chrome.exe                    7352 Console                    4    81 228 Ko
    chrome.exe                    6912 Console                    4    88 404 Ko
    chrome.exe                   19772 Console                    4    22 396 Ko
    chrome.exe                    1736 Console                    4    43 068 Ko
    tasklist.exe                 15236 Console                    4     9 300 Ko
    conhost.exe                   3204 Console                    4    11 004 Ko
    tasklist.exe                 19852 Console                    4     9 004 Ko
```

**Explication:**
- RuntimeBroker.exe : gère les permission des applications Windows universelles, s'assure qu'elles se controle bien. Si ce n'est pas le cas peut utiliser beaucoup de CPU pour régler le problème
- SVCHOST.EXE : consiste à charger des bibliothèques de liens dynamiques pour fournir des fonctions à des applications quand elle en ont besoin
- lsass.exe : Il assure l'identification des utilisateurs
- services.exe :  gère le démarrage et l'arrêt des services Windows, la création et la suppression de ces services
- csrss.exe : gére les fenêtres et les éléments graphiques de Windows


```
PS C:\WINDOWS\system32> Get-Process -includeusername |sort status, username

    Handles      WS(K)   CPU(s)     Id UserName               ProcessName
    [...]
        314       7076     5,80   4600 AUTORITE NT\SERVICE... svchost
        120       3584     0,23   5272 AUTORITE NT\SERVICE... svchost
        127       4060     0,05   4540 AUTORITE NT\SERVICE... svchost
        382      25028   238,69   4536 AUTORITE NT\SERVICE... svchost
        442       9712    53,00   4164 AUTORITE NT\SERVICE... svchost
        139       4120     0,08   4568 AUTORITE NT\SERVICE... svchost
        469      12032     8,06   1920 AUTORITE NT\SERVICE... svchost
        155       7028     8,09   2128 AUTORITE NT\SERVICE... svchost
        376       6300     4,09   3688 AUTORITE NT\SERVICE... svchost
        176       4396     2,30   1776 AUTORITE NT\SERVICE... svchost
        475      14416    14,44   7452 AUTORITE NT\SERVICE... svchost
        427       9040     4,25   2608 AUTORITE NT\SERVICE... dasHost
        148       6168     1,08   1524 AUTORITE NT\SERVICE... svchost
        265       5408     2,34   4108 AUTORITE NT\SERVICE... svchost
        515      10712    30,16   3336 AUTORITE NT\SERVICE... svchost
        267       5864     2,39   3368 AUTORITE NT\SERVICE... svchost
        119       4516     0,13   2332 AUTORITE NT\SERVICE... svchost
        179       5852     1,08   3124 AUTORITE NT\SERVICE... svchost
        184       5912     0,78   2704 AUTORITE NT\SERVICE... svchost
        390       7420    17,66   2712 AUTORITE NT\SERVICE... svchost
        228       6808     2,38   2852 AUTORITE NT\SERVICE... svchost
        154       4716     1,47   1420 AUTORITE NT\SERVICE... svchost
        269       7564     9,83   3896 AUTORITE NT\SERVICE... svchost
        329       5400    39,11   2276 AUTORITE NT\SERVICE... svchost
        221       5844     1,52   1412 AUTORITE NT\SERVICE... svchost
        233       5472     0,17   7232 AUTORITE NT\SERVICE... PresentationFontCache
        139       4712     1,89   3704 AUTORITE NT\SERVICE... svchost
        189       5624     1,64   1404 AUTORITE NT\SERVICE... svchost
        301      12384     7,33   8048 AUTORITE NT\SERVICE... svchost
        350      12232   175,75  20152 AUTORITE NT\SERVICE... WUDFHost
        245       6464     1,50   8320 AUTORITE NT\SERVICE... svchost
        106       5376     0,03  12136 AUTORITE NT\SERVICE... svchost
        199       5460     0,38   8120 AUTORITE NT\SERVICE... svchost
        287      31684   203,88  20076 AUTORITE NT\SERVICE... WUDFHost
        408       8320    15,98   2476 AUTORITE NT\SERVICE... svchost
        180       4808     1,31   2588 AUTORITE NT\SERVICE... svchost
        277       6024     7,31   1320 AUTORITE NT\SERVICE... svchost
        340      16848   168,19  15604 AUTORITE NT\SERVICE... WmiPrvSE
        322       6584   295,41   3692 AUTORITE NT\SERVICE... svchost
        600      16248    92,47   5788 AUTORITE NT\SERVICE... mysqld
        318      12884     9,73   4448 AUTORITE NT\SERVICE... svchost
        237       4828     1,02   4504 AUTORITE NT\SERVICE... NvTelemetryContainer
       1579      16544   219,83   1108 AUTORITE NT\SERVICE... svchost
        124       4580     0,06   4672 AUTORITE NT\SERVICE... mysqld
        125       3636     3,11   5804 AUTORITE NT\SERVICE... conhost
        232       6396     0,55   1536 AUTORITE NT\Système    svchost
        251       7244     2,33   1512 AUTORITE NT\Système    svchost
        145       4072     0,13   4592 AUTORITE NT\Système    XperiaCompanionService
        153       4932     3,72   4520 AUTORITE NT\Système    RstMwService
        129       8072     0,02  10932 AUTORITE NT\Système    unsecapp
        242       5376    10,61   3112 AUTORITE NT\Système    svchost
        337       5984     2,13   4512 AUTORITE NT\Système    RtkAudUService64
        235      11068     0,19   3204 AUTORITE NT\Système    svchost
        475       7892   510,38   1156 AUTORITE NT\Système    svchost
        245       8660     1,83   1740 AUTORITE NT\Système    svchost
        587      29708    33,16  19956 AUTORITE NT\Système    NVDisplay.Container
        485       6988     0,67   1756 AUTORITE NT\Système    svchost
        292       6088     0,20   4528 AUTORITE NT\Système    PsyFrameGrabberService
        153       9164     0,09   4492 AUTORITE NT\Système    QALockHandler
        389       8432    87,59   5896 AUTORITE NT\Système    QASvc
        233      13960    35,75   3480 AUTORITE NT\Système    QAAdminAgent
        435      10880    18,22   1636 AUTORITE NT\Système    svchost
        285      14956   184,19   2644 AUTORITE NT\Système    svchost
         98       3648     0,20   1388 AUTORITE NT\Système    svchost
        249       6060   985,48   4552 AUTORITE NT\Système    vk_service
        102       4068     0,16   4020 AUTORITE NT\Système    wlanext
        144       4840     0,06   4612 AUTORITE NT\Système    sqlwriter
        652      10832     4,70   3916 AUTORITE NT\Système    spoolsv
        150       5860     1,38   2396 AUTORITE NT\Système    svchost
        279      10228     0,59  17804 AUTORITE NT\Système    winlogon
        277       9076    20,22   2560 AUTORITE NT\Système    svchost
        200       8304     5,98   6084 AUTORITE NT\Système    WmiPrvSE
        200       5748     3,48   2844 AUTORITE NT\Système    svchost
        229       5720     0,19   2816 AUTORITE NT\Système    svchost
        176       5964     1,25   2940 AUTORITE NT\Système    svchost
        129       7988     0,11  15808 AUTORITE NT\Système    unsecapp
        267       4060    18,97   2724 AUTORITE NT\Système    svchost
        233       7860   232,70   2692 AUTORITE NT\Système    svchost
        986      46308   117,72   9504 AUTORITE NT\Système    SearchIndexer
       1399      27444   116,66    852 AUTORITE NT\Système    svchost
         54       2808     0,00    416 AUTORITE NT\Système    svchost
        556      22352    23,67   4576 AUTORITE NT\Système    svchost
        219       1100     0,55   7420 AUTORITE NT\Système    DropboxUpdate
        130       3772     0,09   4584 AUTORITE NT\Système    svchost
        412      15632     5,34   4636 AUTORITE NT\Système    svchost
        229       5948     0,94  11684 AUTORITE NT\Système    svchost
        227       9488    13,34  12768 AUTORITE NT\Système    GoProDeviceDetection
        108       4848     0,20  13228 AUTORITE NT\Système    svchost
        238       7480     0,94  13208 AUTORITE NT\Système    svchost
        481      16528   386,25   4560 AUTORITE NT\Système    svchost
        214       7356    11,58  13044 AUTORITE NT\Système    svchost
        207       5992     0,91   4936 AUTORITE NT\Système    svchost
        507      18428    56,25   7524 AUTORITE NT\Système    svchost
        789      14572     9,89   8788 AUTORITE NT\Système    svchost
        735      22604   123,47   8768 AUTORITE NT\Système    svchost
         96       3284     0,02   4100 AUTORITE NT\Système    conhost
        227       7348     7,67   8288 AUTORITE NT\Système    svchost
        367       7232     2,09   5296 AUTORITE NT\Système    svchost
        405       7632     0,70   5280 AUTORITE NT\Système    svchost
        367      10656     3,47   8872 AUTORITE NT\Système    svchost
        138       4220     0,05  12104 AUTORITE NT\Système    DbxSvc
        188       4712     0,16   7512 AUTORITE NT\Système    svchost
         43       2884     0,16    932 AUTORITE NT\Système    LsaIso
       1697      16952   423,30    940 AUTORITE NT\Système    lsass
        312       9392   516,09  15840 AUTORITE NT\Système    AppleMobileDeviceService
        605      15000    13,72   3992 AUTORITE NT\Système    svchost
        745      18592     6,92   4496 AUTORITE NT\Système    KiteService
        205       4388   245,09   4464 AUTORITE NT\Système    mDNSResponder
        273      10128     0,14  19332 AUTORITE NT\Système    svchost
        271       8868     5,27   2460 AUTORITE NT\Système    NVDisplay.Container
        199       5388     0,09   7948 AUTORITE NT\Système    svchost
        330      10140     2,48  15340 AUTORITE NT\Système    svchost
        471      53972    34,05   8976 AUTORITE NT\Système    MoUsoCoreWorker
        176       5656     0,27   3040 AUTORITE NT\Système    igfxCUIService
        254       9592     2,06   4088 AUTORITE NT\Système    svchost
        493       8540    19,75   1948 AUTORITE NT\Système    svchost
        456      14692    15,88  13940 AUTORITE NT\Système    IAStorDataMgrSvc
        157       7544    88,72   4456 AUTORITE NT\Système    ACCSvc
        138       4128     0,02   5728 AUTORITE NT\Système    jhi_service
        140       4352     0,14   4216 AUTORITE NT\Système    IntelCpHeciSvc
        154       4344     0,03   4480 AUTORITE NT\Système    IntelCpHDCPSvc
        152       9160     0,05   8508 AUTORITE NT\Système    igfxext
    [...]
```


### Network

```
PS C:\Users\AUGIER de MOUSSAC> Get-NetAdapter -IncludeHidden

    Name                      InterfaceDescription                    ifIndex Status       MacAddress          LinkSpeed
    ----                      --------------------                    ------- ------       ----------          ---------
    Ethernet (débogueur du... Microsoft Kernel Debug Network Adapter       19 Not Present                          0 bps
    Connexion au réseau l...4 WAN Miniport (IKEv2)                         18 Disconnected                         0 bps
    Connexion au réseau l...5 WAN Miniport (L2TP)                          17 Disconnected                         0 bps
    Connexion au réseau l...3 Microsoft Wi-Fi Direct Virtual Ada...#2      16 Disconnected 1A-56-80-70-9C-48       0 bps
    Connexion au réseau l...7 WAN Miniport (PPPOE)                         15 Disconnected                         0 bps
    Connexion au réseau l...9 WAN Miniport (IPv6)                          14 Up                                   0 bps
    Connexion au réseau l...6 WAN Miniport (PPTP)                          13 Disconnected                         0 bps
    Connexion au réseau l...1 WAN Miniport (SSTP)                          11 Disconnected                         0 bps
    Teredo Tunneling Pseud...                                              10 Not Present                          0 bps
    Connexion au réseau l...2 Microsoft Wi-Fi Direct Virtual Adapter        9 Disconnected 18-56-80-70-9C-49       0 bps
    Connexion au réseau ...10 WAN Miniport (Network Monitor)                7 Up                                   0 bps
    Ethernet                  Realtek PCIe GbE Family Controller            6 Disconnected 98-28-A6-2C-17-F0       0 bps
    Microsoft IP-HTTPS Pla...                                               5 Not Present                          0 bps
    Wi-Fi                     Intel(R) Wireless-AC 9560 160MHz              4 Up           18-56-80-70-9C-48    780 Mbps
    Connexion au réseau l...8 WAN Miniport (IP)                             3 Up                                   0 bps
    6to4 Adapter                                                            2 Not Present                          0 bps
```

Fonction de chaque carte:
- Microsoft Kernel Debug Network Adapter → virtuel NIC
- WAN Miniport (IKEv2) → cartes virtuelles
- WAN Miniport (L2TP) → cartes virtuelles
- Microsoft Wi-Fi Direct Virtual Ada...#2 → permet de diffuser/partager sa connection internet
- WAN Miniport (PPPOE) → cartes virtuelles
- WAN Miniport (IPv6) → cartes virtuelles
- WAN Miniport (PPTP) → cartes virtuelles
- WAN Miniport (SSTP) → cartes virtuelles
- Microsoft Wi-Fi Direct Virtual Adapter → permet de diffuser/partager sa connection internet
- Realtek PCIe GbE Family Controller → contrôle la fonction de réseau filaire de votre ordinateur. Il s'agit d'un chipset de contrôleur qui interface le bus PCI Express (PCIe) de votre ordinateur avec GBE (Giga Band Ethernet)
- Intel(R) Wireless-AC 9560 160MHz → carte wifi
- WAN Miniport (IP) → cartes virtuelles
- 6to4 Adapter → 6to4 permet aux paquets d'adresses IPv6 d'être contenus dans des paquets IPv4 pour qu'ils puissent être transmis avec succès sur les réseaux IPv4


```
PS C:\WINDOWS\system32> netstat -a -b -p tcp

Connexions actives

  Proto  Adresse locale         Adresse distante       État
  TCP    0.0.0.0:135            LAPTOP-UAMOT1MJ:0      LISTENING
  RpcSs
 [svchost.exe]
  TCP    0.0.0.0:445            LAPTOP-UAMOT1MJ:0      LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    0.0.0.0:3306           LAPTOP-UAMOT1MJ:0      LISTENING
 [mysqld.exe]
  TCP    0.0.0.0:5040           LAPTOP-UAMOT1MJ:0      LISTENING
  CDPSvc
 [svchost.exe]
  TCP    0.0.0.0:5357           LAPTOP-UAMOT1MJ:0      LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    0.0.0.0:7680           LAPTOP-UAMOT1MJ:0      LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    0.0.0.0:17500          LAPTOP-UAMOT1MJ:0      LISTENING
 [Dropbox.exe]
  TCP    0.0.0.0:33060          LAPTOP-UAMOT1MJ:0      LISTENING
 [mysqld.exe]
  TCP    0.0.0.0:49664          LAPTOP-UAMOT1MJ:0      LISTENING
 [lsass.exe]
  TCP    0.0.0.0:49665          LAPTOP-UAMOT1MJ:0      LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    0.0.0.0:49666          LAPTOP-UAMOT1MJ:0      LISTENING
  EventLog
 [svchost.exe]
  TCP    0.0.0.0:49667          LAPTOP-UAMOT1MJ:0      LISTENING
  Schedule
 [svchost.exe]
  TCP    0.0.0.0:49668          LAPTOP-UAMOT1MJ:0      LISTENING
  SessionEnv
 [svchost.exe]
  TCP    0.0.0.0:49669          LAPTOP-UAMOT1MJ:0      LISTENING
 [spoolsv.exe]
  TCP    0.0.0.0:49680          LAPTOP-UAMOT1MJ:0      LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    10.10.10.1:139         LAPTOP-UAMOT1MJ:0      LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    127.0.0.1:843          LAPTOP-UAMOT1MJ:0      LISTENING
 [Dropbox.exe]
  TCP    127.0.0.1:5354         LAPTOP-UAMOT1MJ:0      LISTENING
 [mDNSResponder.exe]
  TCP    127.0.0.1:5354         LAPTOP-UAMOT1MJ:49670  ESTABLISHED
 [mDNSResponder.exe]
  TCP    127.0.0.1:5354         LAPTOP-UAMOT1MJ:49672  ESTABLISHED
 [mDNSResponder.exe]
  TCP    127.0.0.1:5354         LAPTOP-UAMOT1MJ:52027  ESTABLISHED
 [mDNSResponder.exe]
  TCP    127.0.0.1:5354         LAPTOP-UAMOT1MJ:52028  ESTABLISHED
 [mDNSResponder.exe]
  TCP    127.0.0.1:6000         LAPTOP-UAMOT1MJ:0      LISTENING
 [foxitphantompdf.exe]
  TCP    127.0.0.1:6463         LAPTOP-UAMOT1MJ:0      LISTENING
 [Discord.exe]
  TCP    127.0.0.1:17600        LAPTOP-UAMOT1MJ:0      LISTENING
 [Dropbox.exe]
  TCP    127.0.0.1:27015        LAPTOP-UAMOT1MJ:0      LISTENING
 [AppleMobileDeviceService.exe]
  TCP    127.0.0.1:27015        LAPTOP-UAMOT1MJ:52026  ESTABLISHED
 [AppleMobileDeviceService.exe]
  TCP    127.0.0.1:46624        LAPTOP-UAMOT1MJ:0      LISTENING
 [kited.exe]
  TCP    127.0.0.1:46624        LAPTOP-UAMOT1MJ:51784  TIME_WAIT
  TCP    127.0.0.1:46624        LAPTOP-UAMOT1MJ:51795  TIME_WAIT
  TCP    127.0.0.1:46624        LAPTOP-UAMOT1MJ:51805  TIME_WAIT
  TCP    127.0.0.1:46624        LAPTOP-UAMOT1MJ:51816  TIME_WAIT
  TCP    127.0.0.1:46624        LAPTOP-UAMOT1MJ:51827  TIME_WAIT
  TCP    127.0.0.1:46624        LAPTOP-UAMOT1MJ:51834  TIME_WAIT
  TCP    127.0.0.1:46624        LAPTOP-UAMOT1MJ:51845  TIME_WAIT
  TCP    127.0.0.1:46624        LAPTOP-UAMOT1MJ:51850  ESTABLISHED
 [kited.exe]
  TCP    127.0.0.1:46624        LAPTOP-UAMOT1MJ:51858  TIME_WAIT
  TCP    127.0.0.1:46624        LAPTOP-UAMOT1MJ:51865  TIME_WAIT
  TCP    127.0.0.1:46624        LAPTOP-UAMOT1MJ:51876  TIME_WAIT
  TCP    127.0.0.1:46624        LAPTOP-UAMOT1MJ:51890  TIME_WAIT
  TCP    127.0.0.1:46624        LAPTOP-UAMOT1MJ:51901  TIME_WAIT
  TCP    127.0.0.1:46624        LAPTOP-UAMOT1MJ:51912  TIME_WAIT
  TCP    127.0.0.1:46624        LAPTOP-UAMOT1MJ:51919  TIME_WAIT
  TCP    127.0.0.1:46624        LAPTOP-UAMOT1MJ:51930  TIME_WAIT
  TCP    127.0.0.1:46624        LAPTOP-UAMOT1MJ:51941  TIME_WAIT
  TCP    127.0.0.1:46624        LAPTOP-UAMOT1MJ:51952  TIME_WAIT
  TCP    127.0.0.1:46624        LAPTOP-UAMOT1MJ:51962  TIME_WAIT
  TCP    127.0.0.1:46624        LAPTOP-UAMOT1MJ:51973  TIME_WAIT
  TCP    127.0.0.1:46624        LAPTOP-UAMOT1MJ:51980  TIME_WAIT
  TCP    127.0.0.1:46624        LAPTOP-UAMOT1MJ:51991  TIME_WAIT
  TCP    127.0.0.1:46624        LAPTOP-UAMOT1MJ:52002  TIME_WAIT
  TCP    127.0.0.1:46624        LAPTOP-UAMOT1MJ:52013  TIME_WAIT
  TCP    127.0.0.1:46624        LAPTOP-UAMOT1MJ:52023  TIME_WAIT
  TCP    127.0.0.1:49670        LAPTOP-UAMOT1MJ:5354   ESTABLISHED
 [AppleMobileDeviceService.exe]
  TCP    127.0.0.1:49672        LAPTOP-UAMOT1MJ:5354   ESTABLISHED
 [AppleMobileDeviceService.exe]
  TCP    127.0.0.1:49681        LAPTOP-UAMOT1MJ:49682  ESTABLISHED
 [mysqld.exe]
  TCP    127.0.0.1:49682        LAPTOP-UAMOT1MJ:49681  ESTABLISHED
 [mysqld.exe]
  TCP    127.0.0.1:51780        LAPTOP-UAMOT1MJ:27015  TIME_WAIT
  TCP    127.0.0.1:51781        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51782        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51783        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51785        LAPTOP-UAMOT1MJ:27015  TIME_WAIT
  TCP    127.0.0.1:51786        LAPTOP-UAMOT1MJ:27015  TIME_WAIT
  TCP    127.0.0.1:51787        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51788        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51789        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51790        LAPTOP-UAMOT1MJ:27015  TIME_WAIT
  TCP    127.0.0.1:51791        LAPTOP-UAMOT1MJ:27015  TIME_WAIT
  TCP    127.0.0.1:51792        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51793        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51794        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51796        LAPTOP-UAMOT1MJ:27015  TIME_WAIT
  TCP    127.0.0.1:51797        LAPTOP-UAMOT1MJ:27015  TIME_WAIT
  TCP    127.0.0.1:51798        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51799        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51800        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51801        LAPTOP-UAMOT1MJ:27015  TIME_WAIT
  TCP    127.0.0.1:51802        LAPTOP-UAMOT1MJ:27015  TIME_WAIT
  TCP    127.0.0.1:51803        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51804        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51806        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51807        LAPTOP-UAMOT1MJ:27015  TIME_WAIT
  TCP    127.0.0.1:51808        LAPTOP-UAMOT1MJ:27015  TIME_WAIT
  TCP    127.0.0.1:51809        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51810        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51811        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51812        LAPTOP-UAMOT1MJ:27015  TIME_WAIT
  TCP    127.0.0.1:51813        LAPTOP-UAMOT1MJ:27015  TIME_WAIT
  TCP    127.0.0.1:51814        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51815        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51817        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51818        LAPTOP-UAMOT1MJ:27015  TIME_WAIT
  TCP    127.0.0.1:51819        LAPTOP-UAMOT1MJ:27015  TIME_WAIT
  TCP    127.0.0.1:51820        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51821        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51822        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51823        LAPTOP-UAMOT1MJ:27015  TIME_WAIT
  TCP    127.0.0.1:51824        LAPTOP-UAMOT1MJ:27015  TIME_WAIT
  TCP    127.0.0.1:51825        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51826        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51828        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51829        LAPTOP-UAMOT1MJ:27015  TIME_WAIT
  TCP    127.0.0.1:51830        LAPTOP-UAMOT1MJ:27015  TIME_WAIT
  TCP    127.0.0.1:51831        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51832        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51833        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51835        LAPTOP-UAMOT1MJ:27015  TIME_WAIT
  TCP    127.0.0.1:51836        LAPTOP-UAMOT1MJ:27015  TIME_WAIT
  TCP    127.0.0.1:51837        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51838        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51839        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51840        LAPTOP-UAMOT1MJ:27015  TIME_WAIT
  TCP    127.0.0.1:51841        LAPTOP-UAMOT1MJ:27015  TIME_WAIT
  TCP    127.0.0.1:51842        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51843        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51844        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51846        LAPTOP-UAMOT1MJ:27015  TIME_WAIT
  TCP    127.0.0.1:51847        LAPTOP-UAMOT1MJ:27015  TIME_WAIT
  TCP    127.0.0.1:51848        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51849        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51850        LAPTOP-UAMOT1MJ:46624  ESTABLISHED
 [kited.exe]
  TCP    127.0.0.1:51851        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51854        LAPTOP-UAMOT1MJ:27015  TIME_WAIT
  TCP    127.0.0.1:51855        LAPTOP-UAMOT1MJ:27015  TIME_WAIT
  TCP    127.0.0.1:51856        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51857        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51859        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51860        LAPTOP-UAMOT1MJ:27015  TIME_WAIT
  TCP    127.0.0.1:51861        LAPTOP-UAMOT1MJ:27015  TIME_WAIT
  TCP    127.0.0.1:51862        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51863        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51864        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51866        LAPTOP-UAMOT1MJ:27015  TIME_WAIT
  TCP    127.0.0.1:51867        LAPTOP-UAMOT1MJ:27015  TIME_WAIT
  TCP    127.0.0.1:51868        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51869        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51870        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51871        LAPTOP-UAMOT1MJ:27015  TIME_WAIT
  TCP    127.0.0.1:51872        LAPTOP-UAMOT1MJ:27015  TIME_WAIT
  TCP    127.0.0.1:51873        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51874        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51875        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51878        LAPTOP-UAMOT1MJ:27015  TIME_WAIT
  TCP    127.0.0.1:51879        LAPTOP-UAMOT1MJ:27015  TIME_WAIT
  TCP    127.0.0.1:51880        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51881        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51882        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51885        LAPTOP-UAMOT1MJ:27015  TIME_WAIT
  TCP    127.0.0.1:51886        LAPTOP-UAMOT1MJ:27015  TIME_WAIT
  TCP    127.0.0.1:51887        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51888        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51889        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51891        LAPTOP-UAMOT1MJ:27015  TIME_WAIT
  TCP    127.0.0.1:51892        LAPTOP-UAMOT1MJ:27015  TIME_WAIT
  TCP    127.0.0.1:51893        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51894        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51896        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51897        LAPTOP-UAMOT1MJ:27015  TIME_WAIT
  TCP    127.0.0.1:51898        LAPTOP-UAMOT1MJ:27015  TIME_WAIT
  TCP    127.0.0.1:51899        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51900        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51902        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51903        LAPTOP-UAMOT1MJ:27015  TIME_WAIT
  TCP    127.0.0.1:51904        LAPTOP-UAMOT1MJ:27015  TIME_WAIT
  TCP    127.0.0.1:51905        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51906        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51907        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51908        LAPTOP-UAMOT1MJ:27015  TIME_WAIT
  TCP    127.0.0.1:51909        LAPTOP-UAMOT1MJ:27015  TIME_WAIT
  TCP    127.0.0.1:51910        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51911        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51913        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51914        LAPTOP-UAMOT1MJ:27015  TIME_WAIT
  TCP    127.0.0.1:51915        LAPTOP-UAMOT1MJ:27015  TIME_WAIT
  TCP    127.0.0.1:51916        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51917        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51918        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51920        LAPTOP-UAMOT1MJ:27015  TIME_WAIT
  TCP    127.0.0.1:51921        LAPTOP-UAMOT1MJ:27015  TIME_WAIT
  TCP    127.0.0.1:51922        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51923        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51924        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51925        LAPTOP-UAMOT1MJ:27015  TIME_WAIT
  TCP    127.0.0.1:51926        LAPTOP-UAMOT1MJ:27015  TIME_WAIT
  TCP    127.0.0.1:51927        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51928        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51929        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51931        LAPTOP-UAMOT1MJ:27015  TIME_WAIT
  TCP    127.0.0.1:51932        LAPTOP-UAMOT1MJ:27015  TIME_WAIT
  TCP    127.0.0.1:51933        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51934        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51935        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51936        LAPTOP-UAMOT1MJ:27015  TIME_WAIT
  TCP    127.0.0.1:51937        LAPTOP-UAMOT1MJ:27015  TIME_WAIT
  TCP    127.0.0.1:51938        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51939        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51940        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51942        LAPTOP-UAMOT1MJ:27015  TIME_WAIT
  TCP    127.0.0.1:51943        LAPTOP-UAMOT1MJ:27015  TIME_WAIT
  TCP    127.0.0.1:51944        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51945        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51946        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51947        LAPTOP-UAMOT1MJ:27015  TIME_WAIT
  TCP    127.0.0.1:51948        LAPTOP-UAMOT1MJ:27015  TIME_WAIT
  TCP    127.0.0.1:51949        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51950        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51951        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51953        LAPTOP-UAMOT1MJ:27015  TIME_WAIT
  TCP    127.0.0.1:51954        LAPTOP-UAMOT1MJ:27015  TIME_WAIT
  TCP    127.0.0.1:51955        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51956        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51957        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51958        LAPTOP-UAMOT1MJ:27015  TIME_WAIT
  TCP    127.0.0.1:51959        LAPTOP-UAMOT1MJ:27015  TIME_WAIT
  TCP    127.0.0.1:51960        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51961        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51963        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51964        LAPTOP-UAMOT1MJ:27015  TIME_WAIT
  TCP    127.0.0.1:51965        LAPTOP-UAMOT1MJ:27015  TIME_WAIT
  TCP    127.0.0.1:51966        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51967        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51968        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51969        LAPTOP-UAMOT1MJ:27015  TIME_WAIT
  TCP    127.0.0.1:51970        LAPTOP-UAMOT1MJ:27015  TIME_WAIT
  TCP    127.0.0.1:51971        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51972        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51974        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51975        LAPTOP-UAMOT1MJ:27015  TIME_WAIT
  TCP    127.0.0.1:51976        LAPTOP-UAMOT1MJ:27015  TIME_WAIT
  TCP    127.0.0.1:51977        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51978        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51979        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51981        LAPTOP-UAMOT1MJ:27015  TIME_WAIT
  TCP    127.0.0.1:51982        LAPTOP-UAMOT1MJ:27015  TIME_WAIT
  TCP    127.0.0.1:51983        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51984        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51985        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51986        LAPTOP-UAMOT1MJ:27015  TIME_WAIT
  TCP    127.0.0.1:51987        LAPTOP-UAMOT1MJ:27015  TIME_WAIT
  TCP    127.0.0.1:51988        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51989        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51990        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51992        LAPTOP-UAMOT1MJ:27015  TIME_WAIT
  TCP    127.0.0.1:51993        LAPTOP-UAMOT1MJ:27015  TIME_WAIT
  TCP    127.0.0.1:51994        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51995        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51996        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:51997        LAPTOP-UAMOT1MJ:27015  TIME_WAIT
  TCP    127.0.0.1:51998        LAPTOP-UAMOT1MJ:27015  TIME_WAIT
  TCP    127.0.0.1:51999        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:52000        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:52001        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:52003        LAPTOP-UAMOT1MJ:27015  TIME_WAIT
  TCP    127.0.0.1:52004        LAPTOP-UAMOT1MJ:27015  TIME_WAIT
  TCP    127.0.0.1:52005        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:52006        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:52007        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:52008        LAPTOP-UAMOT1MJ:27015  TIME_WAIT
  TCP    127.0.0.1:52009        LAPTOP-UAMOT1MJ:27015  TIME_WAIT
  TCP    127.0.0.1:52010        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:52011        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:52012        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:52014        LAPTOP-UAMOT1MJ:27015  TIME_WAIT
  TCP    127.0.0.1:52015        LAPTOP-UAMOT1MJ:27015  TIME_WAIT
  TCP    127.0.0.1:52016        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:52017        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:52018        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:52019        LAPTOP-UAMOT1MJ:27015  TIME_WAIT
  TCP    127.0.0.1:52020        LAPTOP-UAMOT1MJ:27015  TIME_WAIT
  TCP    127.0.0.1:52021        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:52022        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:52024        LAPTOP-UAMOT1MJ:5354   TIME_WAIT
  TCP    127.0.0.1:52025        LAPTOP-UAMOT1MJ:27015  TIME_WAIT
  TCP    127.0.0.1:52026        LAPTOP-UAMOT1MJ:27015  ESTABLISHED
 [duet.exe]
  TCP    127.0.0.1:52027        LAPTOP-UAMOT1MJ:5354   ESTABLISHED
 [AppleMobileDeviceService.exe]
  TCP    127.0.0.1:52028        LAPTOP-UAMOT1MJ:5354   ESTABLISHED
 [AppleMobileDeviceService.exe]
  TCP    127.0.0.1:56550        LAPTOP-UAMOT1MJ:56551  ESTABLISHED
 [Dropbox.exe]
  TCP    127.0.0.1:56551        LAPTOP-UAMOT1MJ:56550  ESTABLISHED
 [Dropbox.exe]
  TCP    127.0.0.1:57171        LAPTOP-UAMOT1MJ:57172  ESTABLISHED
 [Dropbox.exe]
  TCP    127.0.0.1:57172        LAPTOP-UAMOT1MJ:57171  ESTABLISHED
 [Dropbox.exe]
  TCP    192.168.0.26:139       LAPTOP-UAMOT1MJ:0      LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    192.168.0.26:50720     162.125.19.131:https   ESTABLISHED
 [Dropbox.exe]
  TCP    192.168.0.26:50953     162.125.19.9:https     ESTABLISHED
 [Dropbox.exe]
  TCP    192.168.0.26:51587     162.159.136.232:https  ESTABLISHED
 [Discord.exe]
  TCP    192.168.0.26:51766     51.138.106.75:https    ESTABLISHED
  CDPUserSvc_8b06211d
 [svchost.exe]
  TCP    192.168.0.26:51877     ec2-3-88-238-71:https  TIME_WAIT
  TCP    192.168.0.26:51895     162.125.6.20:https     ESTABLISHED
 [Dropbox.exe]
  TCP    192.168.0.26:57004     40.67.254.36:https     ESTABLISHED
  WpnService
 [svchost.exe]
  TCP    192.168.0.26:57011     162.159.133.234:https  ESTABLISHED
 [Discord.exe]
  TCP    192.168.0.26:57176     ec2-52-201-44-25:https  ESTABLISHED
 [chrome.exe]
  TCP    192.168.0.26:64982     59:https               ESTABLISHED
 [kited.exe]
  TCP    192.168.56.1:139       LAPTOP-UAMOT1MJ:0      LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    192.168.120.1:139      LAPTOP-UAMOT1MJ:0      LISTENING
 Impossible d’obtenir les informations de propriétaire
```

**Explication:**
- svchost.exe : consiste à charger des bibliothèques de liens dynamiques pour fournir des fonctions à des applications quand elle en ont besoin
- lsass.exe : Il assure l'identification des utilisateurs
- mysqld.exe : processus nécessaire au bon fonctionnement de l'application MySQL Server
- Dropbox.exe : processus nécessaire au bon fonctionnement de l'application Dropbox
- spoolsv.exe : processus générique de Windows. Il met en cache les travaaux d'impession.
- foxitphantompdf.exe : processus nécessaire au bon fonctionnement de l'application Foxit Phantom PDF
- Discord.exe : processus nécessaire au bon fonctionnement de l'application Discord
- kited.exe : processus nécessaire au bon fonctionnement de l'extention Kite sur Visual Studio Code



### Gestion de softs

**Les intérêt de l'utilisation d'un gestionnaire de paquets :**
- pour un téléchargement direct sur internet: 
    - La centralisation de tous les logiciel sur un même serveur permet d'installer et de mettre à jour tous les logiciels que l'ont veux avec une ligne de commande. Donc plus besoin de télécharger le logiciel voulu sur chaque site. 
    - Si un logiciel dépend d'un programme pour fonctionner, le gestionnaire de paquets télechargera le logiciel et son programme associé automatiquement
    - Aucun logiciel pré-cochez sera installer 
    
- selon l'identité des gens impliqués dans un téléchargement :
    - Un gestionnaire paquet permet à l'utilisateur un gain de temps dans l'installation/désinstallation et la mise à jour des ses applications mais aussi plus de sécurité  


- pour la sécurité globale impliquée lors d'un téléchargement:
    - Un gestionnaire de paquet évite que l'utilisateur ne télécharge un spywares, malwares ou adwares depuis un site malveillant. Donc plus de sécuritée
    - Le gestionnaire ne stocke aucune informations concernant l'utilisateur


** Liste des paquet installés**

```
PS C:\Users\AUGIER de MOUSSAC> winget list

Nom                                          ID                                                     Version
-----------------------------------------------------------------------------------------------------------------------
Dame de Pique                                26720RandomSaladGamesLLC.HeartsDeluxe_kx24dqmazqk8j    6.7.33.0
Simple Mahjong                               26720RandomSaladGamesLLC.SimpleMahjong_kx24dqmazqk8j   5.8.35.0
Solitaire Français                           26720RandomSaladGamesLLC.SimpleSolitaire_kx24dqmazqk8j 6.20.87.0
Spades                                       26720RandomSaladGamesLLC.Spades_kx24dqmazqk8j          6.0.60.0
Netflix                                      4DF9E0F8.Netflix_mcm4njqhnhss8                         6.97.752.0
WhatsApp Desktop                             5319275A.WhatsAppDesktop_cv1g1gvanyjgm                 2.2043.22.0
Slack                                        91750D7E.Slack_8she8kybcnzg4                           4.10.3.0
HP Smart                                     AD2F1837.HPPrinterControl_v10z8vjag6ke6                121.1.193.0
Care Center                                  AcerIncorporated.AcerCareCenter_48frkmn4z8aw4          3.0.3007.0
Acer Collection S                            AcerIncorporated.AcerCollectionS_48frkmn4z8aw4         1.0.3004.0
Acer Product Registration                    AcerIncorporated.AcerRegistration_48frkmn4z8aw4        2.0.3013.0
QuickAccess                                  AcerIncorporated.QuickAccess_48frkmn4z8aw4             3.0.3017.0
User Experience Improvement Program          AcerIncorporated.UserExperienceImprovementProgram_48f… 4.0.3004.0
Intel® Graphics Control Panel                AppUp.IntelGraphicsControlPanel_8j3eq9eme6ctt          3.3.0.0
Ubuntu 20.04 LTS                             CanonicalGroupLimited.Ubuntu20.04onWindows_79rhkp1fnd… 2004.2020.812.0
PhotoDirector for acer                       CyberLinkCorp.ac.PhotoDirectorforacerDesktop_ypz87dpx… 8.0.5229.0
PowerDirector for acer                       CyberLinkCorp.ac.PowerDirectorforacerDesktop_ypz87dpx… 14.0.4304.0
Evernote                                     Evernote.Evernote_q4d96b2w5wcc2                        10.1.7.0
Foxit PhantomPDF                             FF536CFE.5499862B856D1_6y892r7ea31b2                   9.7.30114.0
Movie & Audio Studio                         MAGIXSoftwareGmbH.MovieAudioStudio_awcgk3qbzve1y       1.1.4.0
Cortana                                      Microsoft.549981C3F5F10_8wekyb3d8bbwe                  2.2010.22653.0
Traducteur                                   Microsoft.BingTranslator_8wekyb3d8bbwe                 5.6.0.0
MSN Météo                                    Microsoft.BingWeather_8wekyb3d8bbwe                    4.46.22742.0
Programme d'installation d'application       Microsoft.DesktopAppInstaller_8wekyb3d8bbwe            1.11.2941.0
Feathers                                     Microsoft.Feathers_8wekyb3d8bbwe                       1.0.0.0
Obtenir de l'aide                            Microsoft.GetHelp_8wekyb3d8bbwe                        10.2004.31291.0
Astuces Microsoft                            Microsoft.Getstarted_8wekyb3d8bbwe                     9.12.32951.0
Extensions d'image HEIF                      Microsoft.HEIFImageExtension_8wekyb3d8bbwe             1.0.32532.0
Module d'expérience locale français (France) Microsoft.LanguageExperiencePackfr-FR_8wekyb3d8bbwe    19041.12.29.0
Paint 3D                                     Microsoft.MSPaint_8wekyb3d8bbwe                        6.2009.30067.0
Visionneuse 3D                               Microsoft.Microsoft3DViewer_8wekyb3d8bbwe              7.2009.29132.0
Microsoft Edge                               Microsoft.MicrosoftEdge.Stable_8wekyb3d8bbwe           86.0.622.63
Microsoft Mahjong                            Microsoft.MicrosoftMahjong_8wekyb3d8bbwe               4.0.9220.0
Office                                       Microsoft.MicrosoftOfficeHub_8wekyb3d8bbwe             18.2008.12711.0
Microsoft Solitaire Collection               Microsoft.MicrosoftSolitaireCollection_8wekyb3d8bbwe   4.7.10142.0
Pense-bêtes Microsoft                        Microsoft.MicrosoftStickyNotes_8wekyb3d8bbwe           3.7.142.0
Portail de réalité mixte                     Microsoft.MixedReality.Portal_8wekyb3d8bbwe            2000.20081.1312.0
Microsoft Office Desktop Apps                Microsoft.Office.Desktop_8wekyb3d8bbwe                 16051.13328.20292.0
OneNote for Windows 10                       Microsoft.Office.OneNote_8wekyb3d8bbwe                 16001.13328.20348.0
Contacts Microsoft                           Microsoft.People_8wekyb3d8bbwe                         10.1909.10841.0
Print 3D                                     Microsoft.Print3D_8wekyb3d8bbwe                        3.3.791.0
Bureau à distance Microsoft                  Microsoft.RemoteDesktop_8wekyb3d8bbwe                  10.2.1535.0
Capture d'écran et croquis                   Microsoft.ScreenSketch_8wekyb3d8bbwe                   10.2008.2277.0
Skype                                        Microsoft.SkypeApp_kzf8qxf38zg5c                       15.65.78.0
Hôte de l'expérience du Windows Store        Microsoft.StorePurchaseApp_8wekyb3d8bbwe               12011.1001.1.0
Extensions vidéo VP9                         Microsoft.VP9VideoExtensions_8wekyb3d8bbwe             1.0.32521.0
Microsoft Pay                                Microsoft.Wallet_8wekyb3d8bbwe                         2.4.18324.0
Extensions de support web                    Microsoft.WebMediaExtensions_8wekyb3d8bbwe             1.0.20875.0
Extensions d'image Webp                      Microsoft.WebpImageExtension_8wekyb3d8bbwe             1.0.32731.0
Photos Microsoft                             Microsoft.Windows.Photos_8wekyb3d8bbwe                 2020.20090.1002.0
Alarmes et horloge Windows                   Microsoft.WindowsAlarms_8wekyb3d8bbwe                  10.2009.5.0
Calculatrice Windows                         Microsoft.WindowsCalculator_8wekyb3d8bbwe              10.2009.4.0
Caméra Windows                               Microsoft.WindowsCamera_8wekyb3d8bbwe                  2020.504.60.0
Hub de commentaires                          Microsoft.WindowsFeedbackHub_8wekyb3d8bbwe             1.1907.3152.0
Cartes Windows                               Microsoft.WindowsMaps_8wekyb3d8bbwe                    10.2009.2.0
Enregistreur vocal Windows                   Microsoft.WindowsSoundRecorder_8wekyb3d8bbwe           10.2009.2.0
Microsoft Store                              Microsoft.WindowsStore_8wekyb3d8bbwe                   12010.1001.3.0
WinGet Source                                Microsoft.Winget.Source_8wekyb3d8bbwe                  2020.1109.1744.538
Xbox TCUI                                    Microsoft.Xbox.TCUI_8wekyb3d8bbwe                      1.24.10001.0
Compagnon de la console Xbox                 Microsoft.XboxApp_8wekyb3d8bbwe                        48.70.21001.0
Xbox Game Bar Plugin                         Microsoft.XboxGameOverlay_8wekyb3d8bbwe                1.54.4001.0
Xbox Game Bar                                Microsoft.XboxGamingOverlay_8wekyb3d8bbwe              5.420.10222.0
Xbox Identity Provider                       Microsoft.XboxIdentityProvider_8wekyb3d8bbwe           12.67.21001.0
Xbox Game Speech Window                      Microsoft.XboxSpeechToTextOverlay_8wekyb3d8bbwe        1.21.13002.0
Votre téléphone                              Microsoft.YourPhone_8wekyb3d8bbwe                      1.20101.97.0
Groove Musique                               Microsoft.ZuneMusic_8wekyb3d8bbwe                      10.20092.10311.0
Films et TV                                  Microsoft.ZuneVideo_8wekyb3d8bbwe                      10.20082.10421.0
NVIDIA Control Panel                         NVIDIACorp.NVIDIAControlPanel_56jybvy8sckqj            8.1.958.0
Realtek Audio Control                        RealtekSemiconductorCorp.RealtekAudioControl_dt26b99r… 1.1.137.0
SoundCloud for Windows (Beta)                SoundcloudLtd.SoundCloudforWindowsBeta_2xc63xn306dnw   1.1.36.0
VLC                                          VideoLAN.VLC_paz6r1rewnh0a                             3.2.1.0
WildTangent Games                            WildTangentGames.63435CFB65F55_qt5r5pa5dyg8m           2.0.82.0
Courrier et calendrier                       microsoft.windowscommunicationsapps_8wekyb3d8bbwe      16005.13228.41011.0
```


```
PS C:\Users\AUGIER de MOUSSAC> winget source list
Nom    Argument
-----------------------------------------
winget https://winget.azureedge.net/cache
```




### Partage de fichiers

```
PS C:\WINDOWS\system32> New-SmbShare -Name PartageDeFichierWindowClient -Path "D:\Ynov\Windows client" -FullAccess "Tout le monde"

Name                         ScopeName Path                   Description
----                         --------- ----                   -----------
PartageDeFichierWindowClient *         D:\Ynov\Windows client
```

```
PS C:\Users\AUGIER de MOUSSAC> ssh root@192.168.120.50
root@192.168.120.50's password:
Last login: Mon Nov  9 16:36:27 2020
[root@localhost ~]# yum install -y cifs-utils
Loaded plugins: fastestmirror
Loading mirror speeds from cached hostfile
 * base: mirroir.wptheme.fr
 * extras: ftp.rezopole.net
 * updates: mirroir.wptheme.fr
base                                                                                             | 3.6 kB  00:00:00
extras                                                                                           | 2.9 kB  00:00:00
updates                                                                                          | 2.9 kB  00:00:00
Package cifs-utils-6.2-10.el7.x86_64 already installed and latest version
Nothing to do
```

Ayant fait la manipulation 2 fois, le dossier était déjà présent 
```
[root@localhost ~]# mkdir /opt/partage
mkdir: cannot create directory ‘/opt/partage’: File exists
```

```
[root@localhost ~]# mount -t cifs -o username="augier de moussac",password=.......... //192.168.120.1/PartageDeFichierWindowCl
ient /opt/partage
```

On affiche les fichier présent dans le dossier partagé
```
[root@localhost ~]# cd /opt/partage
[root@localhost partage]# ls -a
.  ..  desktop.ini  teste partage de fichier.txt
```

On créer un nouveau fichier(nommé quelquqechose) depuis la Machine virtuel 
```
[root@localhost partage]# touch quelquqechose
```